#!/bin/bash
# Usagge:
#	bash launch_svn2git_import.sh svn_repo svn_root_path svn_sub_repo (optional)
# Examples:
#	bash launch_svn2git_import.sh openidm /var/www/svn
#	bash launch_svn2git_import.sh openicf /var/www/svn java-framework
# The new Git repo will be imported in the current folder
# When the migration will be completed, it will be moved in a new folder (date_time_repo)

[[ $# -lt 2 ]] && echo "wrong parameters" && exit
export svn_repo=$1
export svn_root_path=${2%/}
export svn_sub_repo=$3
export rules_dir=$(realpath ../rules)
export authors_file=$(realpath ../authors.txt)
export output_base='/data/'


# Test if a sub-repo is defined (to build the rule filename and the output folder)
if [ -z "$svn_sub_repo" ]
then
	export svn_import_rules=$svn_repo.rules
	export output_dir=$svn_repo
else
	export svn_import_rules=$svn_repo-$svn_sub_repo.rules
	export output_dir=$svn_repo-$svn_sub_repo
fi

ERROR=0

if [ -e $rules_dir/$svn_import_rules ] ; then
  echo "rules are in : $rules_dir/$svn_import_rules"
else
  echo "rules not found in $rules_dir/$svn_import_rules"
  ERROR=1
fi

if [ -d $svn_root_path/$svn_repo ] ; then
  echo "SVN repo being imported is $svn_root_path/$svn_repo"
else
  echo "SVN repo folder not found ($svn_root_path/$svn_repo)"
  ERROR=1
fi

# Create and jump to the import dir
rm -rvf $output_base/$output_dir
mkdir $output_base/$output_dir -p

if [ -d $output_base/$output_dir ] ; then
  echo "output Git repo is at $output_base/$output_dir"
else
  echo "Output dir $output_base/$output_dir does not exists"
  ERROR=1
fi

[ ${ERROR} -eq 1 ] && echo "blocking errors detected: abort." && exit

read -p 'any key will continue...'
echo 'starting migration.'

cd $output_base/$output_dir
# Import the SVN repo

#echo "Output folder: $output_base/$output_dir"
time svn-all-fast-export --identity-map $authors_file --rules $rules_dir/$svn_import_rules --add-metadata-notes --stats --debug-rules $svn_root_path/$svn_repo >& ./$svn_repo.log
#svn-all-fast-export --identity-map /home/martin/gitmove/authors.txt --rules /home/martin/gitmove/rules/spago4q.rules --add-metadata-notes --stats --debug-rules /home/martin/ow2-svn/svnroot//spago4q
#time svn-all-fast-export --identity-map authors.txt --rules standardlayout.rules --add-metadata-notes --stats --debug-rules ~/ow2-svn/svnroot/spago4q/ >& ./spago4q.log
