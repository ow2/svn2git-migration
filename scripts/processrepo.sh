#!/bin/bash

# svn2gitnet...

# remove unwanted tags
git tag v2.3.1-p1 svn/tags/v2.3.1-p1
git tag -d svn/tags/v2.3.1-p1
git for-each-ref 'refs/tags/' --format='%(refname:short)' | grep -v '^v2.*' | xargs git tag -d

git remote add origin git@gitlabtest.ow2.org:clif/clif-test.git

#git branch --set-upstream-to=origin/master master

git push -u origin --all
git push -u origin --tags

#git branch --set-upstream-to=origin/master master
#git remote add origin git@gitlabtest.ow2.org:lemonldap-20171010/llng.git
#git pull --allow-unrelated-histories
#git push -u origin --all
#git push -u origin --tags 
